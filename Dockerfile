FROM python:3.9.2-slim as build
COPY . /app

WORKDIR /app
RUN python setup.py clean --all install

FROM python:3.9.2-alpine3.13
COPY --from=build /usr/local/lib/python3.9/site-packages /usr/local/lib/python3.9/site-packages

RUN pip3 install gunicorn

ENTRYPOINT ["gunicorn"]
CMD ["overseer:create_app()", "--bind", "0.0.0.0:5000", "--threads=4"]
