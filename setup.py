from setuptools import setup, find_packages

setup(
    name="overseer",
    description="Port Scanning Web Interface",
    author="Evan Reichard",
    version="0.0.1",
    packages=find_packages("src"),
    package_dir={"": "src"},
    zip_safe=False,
    include_package_data=True,
    entry_points={"console_scripts": ["overseer = overseer:cli"]},
    install_requires=[
        "flask_socketio",
        "sqlalchemy",
        "Flask",
    ],
    tests_require=["pytest"],
    extras_require={"dev": ["pre-commit", "black", "flake8", "pytest"]},
)
