import Vue from 'vue';
import App from './App.vue';
import store from './store';
import router from './router';
import VueSocketIO from 'vue-socket.io';
import socketio from 'socket.io-client';

/**
 * Initiates the Websocket connection with the API server.
 **/
Vue.use(new VueSocketIO({
    debug: true,
    connection: socketio({ path: "/api/v1/socket.io" }),
    vuex: {
        store,
        actionPrefix: 'SOCKET_',
        mutationPrefix: 'SOCKET_'
    },
}))

Vue.config.productionTip = false

/**
 * Creates the Overseer Vue Application.
 **/
new Vue({
    render: h => h(App),
    router,
    store
}).$mount('#app')
