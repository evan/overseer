import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Scan from '../views/Scan.vue'
import Search from '../views/Search.vue'
import NotFound from '../views/NotFound.vue'

Vue.use(VueRouter)

/**
 * Define all routes within the Overseer SPA.
 **/
const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/scan/:target/:scan_id',
    name: 'Scan',
    component: Scan
  },
  {
    path: '/scan/:target',
    name: 'Scan',
    component: Scan
  },
  {
    path: '/search/:target',
    name: 'Search',
    component: Search
  },
  {
    path: '*',
    name: 'NotFound',
    component: NotFound
  }
]

const router = new VueRouter({
  mode: 'history',
  base: '/',
  routes
})

export default router
