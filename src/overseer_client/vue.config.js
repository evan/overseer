module.exports = {
    publicPath: "/static/",
    outputDir: "../overseer/static/",
    indexPath: "../templates/index.html"
}
