import click
import signal
import sys
from importlib.metadata import version
from overseer.config import Config
from overseer.scanner import ScanManager
from overseer.database import DatabaseConnector
from flask import Flask
from flask.cli import FlaskGroup

__version__ = version("overseer")

app = Flask(__name__)
database = DatabaseConnector(Config.DB_TYPE, Config.DATA_PATH)
scan_manager = ScanManager()


def signal_handler(sig, frame):
    scan_manager.stop()
    sys.exit(0)


def create_app():
    import overseer.api.common as api_common
    import overseer.api.v1 as api_v1

    app.register_blueprint(api_common.bp)
    app.register_blueprint(api_v1.bp)

    scan_manager.start()
    return app


@click.group(cls=FlaskGroup, create_app=create_app)
def cli():
    """Management script for the application."""


# Handle SIGINT
signal.signal(signal.SIGINT, signal_handler)
