from flask import make_response, render_template, send_from_directory
from flask import Blueprint

# Setup blueprint
bp = Blueprint("common", __name__)


@bp.route("/", methods=["GET"])
def main_entry():
    """Initial Entrypoint to the SPA (i.e. 'index.html')"""
    return make_response(render_template("index.html"))


@bp.route("/<path:path>", methods=["GET"])
def catch_all(path):
    """Necessary due to client side SPA route handling"""
    return make_response(render_template("index.html"))


@bp.route("/static/<path:path>")
def static_resources(path):
    """Front end static resources"""
    return send_from_directory("static", path)


# Version API's
# app.register_blueprint(api_v1)
