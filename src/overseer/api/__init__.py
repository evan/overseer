open_websockets = []


def send_websocket_event(data):
    """Send an event to all registered websockets.

    Arguments
    ---------
    data : obj
        Data to send over the websocket(s)
    """
    for socket in open_websockets:
        socket.send(data)
