import os


def get_env(key, default=None, required=False):
    """Wrapper for gathering env vars."""
    if required:
        assert key in os.environ, "Missing Environment Variable: %s" % key
    return os.environ.get(key, default)


class Config:
    """Wrap application configurations

    Attributes
    ----------
    DB_TYPE : str
        The specied desired database (default: sqlite)
    DATA_PATH : str
        The path where to store any resources (default: ./)
    """

    DB_TYPE = get_env("OVERSEER_DB_TYPE", default="sqlite")
    DATA_PATH = get_env("OVERSEER_DATA_PATH", default="./")
